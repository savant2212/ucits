(load-extension "ucits")
(define uci (uci_cursor))
(uci_set_confdir uci "./cfg")
(display (uci_lookup uci "test") )
(display "\n")
(display (uci_add_section uci "test" "test_sec"))
(display "\n")
(display (uci_add_section uci "test" "named_sec" "name"))
(display "\n")
(display (uci_lookup uci "test") )
(display "\n")
(display (uci_delete uci "test.@test_sec[-1]"))
(newline)
(display (uci_delete uci "test.@named_sec[-1]"))
(display "\n")
(display (uci_lookup uci "test") )
(display "\n")
;(uci_commit uci "test")
(display "\n")
