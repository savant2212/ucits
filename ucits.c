#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <scheme-private.h>
#include <uci.h>

pointer ucits_error;
pointer ucits_string;
pointer ucits_list;
pointer ucits_section;
pointer ucits_package;

pointer __UCITS_ERROR(scheme* sc, struct uci_context* ctx, char* fmt,...){
	va_list args;
	va_start(args, fmt);
	int size;
	char* buf;
	pointer retval;

	if( ctx != NULL ){
		char* uci_err;
		uci_get_errorstr(ctx, &uci_err, fmt);
		size = vsnprintf(NULL, 0, uci_err, args) + 1;
		buf = malloc(size);
		vsnprintf(buf, size, uci_err, args);
		free(uci_err);
	} else {
		size = vsnprintf(NULL, 0, fmt, args) + 1;
		buf = malloc(size);
		vsnprintf(buf, size, fmt, args);
	}
	va_end(args);
	retval = cons(sc, ucits_error, mk_string(sc, buf));
	free(buf);
	return retval;
}

#define UCITS_ERROR(s) __UCITS_ERROR(sc, NULL, (s))
#define UCITS_ERROR_CTX(s) __UCITS_ERROR(sc, ctx, (s))
#define UCITS_ERROR_FMT(fmt,...) __UCITS_ERROR(sc, NULL, (fmt), __VA_ARGS__)
#define UCITS_ERROR_CTX_FMT(fmt,...)  __UCITS_ERROR(sc, ctx, (fmt), __VA_ARGS__)

#define UCITS_STRING(s) mk_string(sc,(s)) 
#define UCITS_LIST(lst) cons(sc, ucits_list, (lst)) 
#define UCITS_SECTION( anonymous, type, name, value ) \
	cons(sc, ucits_section, \
			cons(sc,mk_integer(sc,(anonymous)), \
				 cons(sc,mk_string(sc,(type)), \
					 cons(sc,mk_string(sc,(name)), \
						 cons(sc, (value), sc->NIL)))))
#define UCITS_PACKAGE(p) cons(sc, ucits_package, cons( sc, mk_string(sc, p->e.name	),(ucits_sections_to_tslist(sc, &p->sections, &p->sections))));

inline int is_list(scheme *sc, pointer a)
{ return list_length(sc,a) >= 0; }

pointer ucits_cursor(scheme* sc, pointer args){
	struct uci_context* ctx;

	ctx = uci_alloc_context();

	if( ctx == NULL ){
		return UCITS_ERROR("UCI context allocation failed");
	}

	return mk_integer(sc,(unsigned long)ctx);
}

pointer ucits_teardown(scheme* sc, pointer args){
	if( args != sc->NIL ) {
		if( is_number(pair_car(args))) {
			struct uci_context* ctx = (struct uci_context*)ivalue(pair_car(args));
			uci_free_context(ctx);
			return sc->T;
		}
	}

	return sc->F;
}

pointer uci_list_to_tslist(scheme* sc, void* head, struct uci_list* list){
	struct uci_element* elt = list_to_element(list->next);

	if( &elt->list == head ){
		return sc->NIL;
	} 
	return cons(
				sc, 
				UCITS_STRING(elt->name),
					uci_list_to_tslist(sc, head, &elt->list)
				);	
}

pointer ucits_get_option(scheme* sc, struct uci_option* opt ){
	switch( opt->type ){
		case UCI_TYPE_STRING:
			return cons(sc, mk_string(sc, opt->e.name), mk_string(sc, opt->v.string));
			break;
		case UCI_TYPE_LIST:
			return cons(sc, mk_string(sc, opt->e.name), uci_list_to_tslist(sc, &opt->v.list, &opt->v.list));
			break;
	}

	return UCITS_ERROR_FMT("Invalid UCI pointer type %d", opt->type);
}

pointer uci_options_to_tslist(scheme* sc, void* head, struct uci_list* list){
	struct uci_element* elt = list_to_element(list->next);

	struct uci_option* o = uci_to_option(elt);

	if( &elt->list == head ){
		return sc->NIL;
	} 
	
	return cons(
				sc, 
				ucits_get_option( sc, o ),
				uci_options_to_tslist(sc, head, &elt->list)
				);
}

pointer ucits_get_section(scheme* sc, struct uci_section* s ){
	return UCITS_SECTION( s->anonymous, s->type, s->e.name, uci_options_to_tslist(sc, &s->options, &s->options));
}

pointer ucits_sections_to_tslist(scheme* sc, void* head, struct uci_list* list){
	struct uci_element* elt = list_to_element(list->next);

	struct uci_section* s = uci_to_section(elt);

	if( &elt->list == head ){
		return sc->NIL;
	} 
	
	return cons(
				sc, 
				ucits_get_section( sc, s ),
				ucits_sections_to_tslist(sc, head, &elt->list)
				);
}

pointer ucits_get_package(scheme* sc, struct uci_package* p){
	return UCITS_PACKAGE(p);
}

pointer ucits_set(scheme* sc, pointer args){
	struct uci_context* ctx;
	char* selector;
	char* name;
	pointer value;
	struct uci_ptr ptr;
	pointer p;
	int ret;

	if( args == sc->NIL ) 
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);

	if( args == sc->NIL ) return UCITS_ERROR("Second argument must be selector");
	
	p = pair_car(args);
	args = pair_cdr(args);
	
	if( !is_string(p)) return UCITS_ERROR("Second argument must be string");

	selector = strdupa(string_value(p));
	
	if( args == sc->NIL ) return UCITS_ERROR("Third argument must be string");
	p = pair_car(args);
	args = pair_cdr(args);
	
	if( !is_string(p)) return UCITS_ERROR("Second argument must be string");
	
	name = strdup(string_value(p));

	if( args == sc->NIL ) return UCITS_ERROR("Fourth argument must be string or list");
	value = pair_car(args);

	uci_parse_ptr(ctx, &ptr, selector);
	ptr.option = name;

	ret = uci_lookup_ptr(ctx, &ptr, NULL, 1);

	if( ret != UCI_OK) {
		// failed
		return UCITS_ERROR_CTX("Lookup failed");
	}
	
	if( ptr.flags & UCI_LOOKUP_COMPLETE == 0 ){
		//not found
		return UCITS_ERROR("Not found");
	}

	switch ( ptr.last->type ){
		case UCI_TYPE_PACKAGE:
			return UCITS_ERROR("Section not found");
			break;
		case UCI_TYPE_SECTION:
		case UCI_TYPE_OPTION:
			ptr.option = name;
			if( is_list(sc,value) ){
				if( ptr.o ){
					uci_delete(ctx, &ptr);
				}
				while( (p = pair_car(value)) != sc->NIL ){
					if( !is_string( p ) ) {
						value = pair_cdr(value);
						continue;
					}
					ptr.value = string_value(p);
					if( uci_add_list(ctx, &ptr) ){
						//set failed. silent.
					}
					value = pair_cdr(value);
				}
			} else if( is_string(value) ) {
				ptr.value = string_value(value);
				if( uci_set( ctx, &ptr) ){
					return UCITS_ERROR_CTX("Set failed");
				}
			} else {
				return UCITS_ERROR("Value parameter must be string or list of strings");
			}
			break;
		default:
			return UCITS_ERROR("Invalid lookup value");
			break;
	}

	uci_save(ctx, ptr.p);

	return sc->T;
}



/*
 * args: 
 *	1 - ctx pointer
 *	2 - string selector
 */
pointer ucits_lookup_ptr(scheme* sc, pointer args){
	struct uci_context* ctx;
	char* selector;
	struct uci_ptr ptr;
	pointer p;
	int ret;

	if( args == sc->NIL ) 
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);

	if( args == sc->NIL ) return UCITS_ERROR("Second argument must be selector");
	
	p = pair_car(args);
	args = pair_cdr(args);
	
	if( !is_string(p)) return UCITS_ERROR("Second argument must be string");

	selector = string_value(p);

	ret = uci_lookup_ptr(ctx, &ptr, selector, 1);

	if( ret != UCI_OK) {
		// failed
		return UCITS_ERROR_CTX("Lookup failed");
	}
	
	if( ptr.flags & UCI_LOOKUP_COMPLETE == 0 ){
		//not found
		return UCITS_ERROR("Not found");
	}

	switch( ptr.last->type ){
		case UCI_TYPE_PACKAGE:
			return ucits_get_package(sc, ptr.p);
			break;
		case UCI_TYPE_SECTION:
			return ucits_get_section(sc, ptr.s);
			break;
		case UCI_TYPE_OPTION:
			return ucits_get_option(sc, ptr.o);
			break;
	}

	return UCITS_ERROR_FMT( "Invalid UCI pointer type %d", ptr.last->type );
}

pointer ucits_set_confdir(scheme* sc, pointer args) {
	struct uci_context* ctx;
	char* dir;
	pointer p;
	int ret;

	if( args == sc->NIL ) 
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);

	if( args == sc->NIL ) return UCITS_ERROR("Second argument must be selector");
	
	p = pair_car(args);
	args = pair_cdr(args);
	
	if( !is_string(p)) return UCITS_ERROR("Second argument must be string");

	dir = string_value(p);

	if( uci_set_confdir( ctx, dir )) {
		return UCITS_ERROR_CTX( "Conf dir set failed" );
	}

	return sc->T;
}
pointer ucits_set_savedir(scheme* sc, pointer args) {
	struct uci_context* ctx;
	char* dir;
	pointer p;
	int ret;

	if( args == sc->NIL ) 
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);

	if( args == sc->NIL ) return UCITS_ERROR("Second argument must be selector");
	
	p = pair_car(args);
	args = pair_cdr(args);
	
	if( !is_string(p)) return UCITS_ERROR("Second argument must be string");

	dir = string_value(p);

	if( uci_set_save_dir( ctx, dir )) {
		return UCITS_ERROR_CTX( "Save dir set failed" );
	}

	return sc->T;
}
pointer ucits_get_confdir(scheme* sc, pointer args) {
	struct uci_context* ctx;
	pointer p;

	if( args == sc->NIL ) 
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);

	return mk_string(sc, ctx->confdir);

}
pointer ucits_get_savedir(scheme* sc, pointer args) {
	struct uci_context* ctx;
	pointer p;

	if( args == sc->NIL ) 
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);

	return mk_string(sc, ctx->savedir);
}

pointer ucits_commit(scheme* sc, pointer args){
	struct uci_context* ctx;
	pointer p;
	char* conf;
	int ret;
	if( args == sc->NIL )
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);
	p = pair_car(args);
	if( !is_string(p) ) return UCITS_ERROR("Second argument must be string");
	conf = strdupa(string_value(p));

	struct uci_ptr ptr;

	uci_parse_ptr(ctx, &ptr, conf);
	ret = uci_lookup_ptr(ctx, &ptr, NULL, 1);

	if( ret != UCI_OK) {
		// failed
		return UCITS_ERROR_CTX("Lookup failed");
	}
	if( ptr.flags & UCI_LOOKUP_COMPLETE == 0 ){
		//not found
		return UCITS_ERROR("Not found");
	}

	ret = uci_commit(ctx, &ptr.p, 1);
	if( ret != UCI_OK ) {
		return UCITS_ERROR_CTX("Commit failed");
	}

	return sc->T;
}

pointer ucits_delete(scheme* sc, pointer args){
	struct uci_context* ctx;
	pointer p;
	struct uci_ptr ptr;
	char* selector;
	int ret;

	if( args == sc->NIL )
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);

	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);
	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_string(p) ) return UCITS_ERROR("Second argument must be string");
	selector = strdupa(string_value(p));
	uci_parse_ptr(ctx, &ptr, selector);

	ret = uci_lookup_ptr(ctx, &ptr, NULL, 1);

	if( ret != UCI_OK ) return UCITS_ERROR_CTX("Lookup failed");

	ret = uci_delete(ctx, &ptr);

	if( ret != UCI_OK ) return UCITS_ERROR_CTX("Delete failed");

	ret = uci_save(ctx, ptr.p);

	if( ret != UCI_OK ) return UCITS_ERROR_CTX("Saving failed");

	return sc->T;
}

pointer ucits_add_section(scheme* sc, pointer args){
	struct uci_context* ctx;
	pointer p;
	char* pkg;
	char* type;
	char* name;
	struct uci_section* s;
	int ret;
	pointer retval;

	if( args == sc->NIL )
		return UCITS_ERROR("First argument must be uci context");

	p = pair_car(args);
	args = pair_cdr(args);	
	
	if( !is_number(p) ) return UCITS_ERROR("First argument must be number");

	ctx = (struct uci_context*) ivalue(p);
	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_string(p) ) return UCITS_ERROR("Second argument must be string");
	pkg = strdupa(string_value(p));

	p = pair_car(args);
	args = pair_cdr(args);
	if( !is_string(p) ) return UCITS_ERROR("Third argument must be string");
	type = string_value(p);

	p = pair_car(args);

	if( p == sc->NIL ){
		name = NULL;
	} else if (is_string(p) ) {
		name = string_value(p);
	} else {
		return UCITS_ERROR("Invalid name type");
	}

	struct uci_ptr ptr;

	uci_parse_ptr(ctx, &ptr, pkg);
	ret = uci_lookup_ptr(ctx, &ptr, NULL,1);
	
	if( ret != UCI_OK ) return UCITS_ERROR_CTX_FMT("Loading package %s failed", pkg);
	
	if( name != NULL ){
		ptr.section = name;
		ptr.value = type;
		uci_set(ctx, &ptr);
	} else {
		uci_add_section(ctx, ptr.p, type, &s);
	}

	ret = uci_save(ctx, ptr.p);
	if( ret != UCI_OK ) return UCITS_ERROR_CTX("Saving failed");
	
	return sc->T;
}

void init_ucits(scheme* sc) {
	ucits_error		=mk_symbol(sc, "uci_error");
	ucits_string	=mk_symbol(sc, "uci_string");
	ucits_list		=mk_symbol(sc, "uci_list");
	ucits_package	=mk_symbol(sc, "uci_package");
	ucits_section	=mk_symbol(sc, "uci_section");

	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_cursor"),
			mk_foreign_func(sc, ucits_cursor)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_teardown"),
			mk_foreign_func(sc, ucits_teardown)
	);
	
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_lookup"),
			mk_foreign_func(sc, ucits_lookup_ptr)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_get_confdir"),
			mk_foreign_func(sc, ucits_get_confdir)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_set_confdir"),
			mk_foreign_func(sc, ucits_set_confdir)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_get_savedir"),
			mk_foreign_func(sc, ucits_get_savedir)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_set_savedir"),
			mk_foreign_func(sc, ucits_set_savedir)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_set"),
			mk_foreign_func(sc, ucits_set)
	);

	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_commit"),
			mk_foreign_func(sc, ucits_commit)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_add_section"),
			mk_foreign_func(sc, ucits_add_section)
	);
	scheme_define(
			sc,
			sc->global_env,
			mk_symbol(sc, "uci_delete"),
			mk_foreign_func(sc, ucits_delete)
	);
	return;
}

